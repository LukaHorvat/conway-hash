{-# LANGUAGE RecordWildCards, NamedFieldPuns #-}
module Conway where

import Data.Vector.Unboxed (Vector, (!))
import qualified Data.Vector.Unboxed as V
import Control.Monad

data Field = Field { field  :: Vector Bool
                   , width  :: Int
                   , height :: Int }
                   deriving (Eq, Ord, Read, Show)

getAt :: Field -> Int -> Int -> Bool
getAt f@Field{..} x y = field ! (x' + y' * width)
    where x' = x `mod` width
          y' = y `mod` height

coords :: Int -> Field -> (Int, Int)
coords n Field{width} = (r, d)
    where (d, r) = n `divMod` width

neighbors :: Int -> Int -> [(Int, Int)]
neighbors x y = [(x + i, y + j) | i <- [-1..1], j <- [-1..1], i /= 0 || j /= 0]

aliveNeighbors :: Int -> Int -> Field -> Int
aliveNeighbors x y f = length $ filter (uncurry (getAt f)) $ neighbors x y

type Rule = Int -> Bool -> Bool

step :: Rule -> Field -> Field
step rule f@Field{..} = f { field = newField }
    where newField = V.imap each field
          each n = rule alive
              where (x, y) = coords n f
                    alive = aliveNeighbors x y f

conwayRule :: Rule
conwayRule n True  = n == 2 || n == 3
conwayRule n False = n == 3

conway :: Field -> [Field]
conway = iterate (step conwayRule)
