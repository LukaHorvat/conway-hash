{-# LANGUAGE RecordWildCards #-}
module Hash.Conway where

import Data.Text (Text)
import qualified Data.Text as Text
import qualified Data.Text.Encoding as Text
import Data.ByteString (ByteString)
import qualified Data.ByteString as BS
import Data.Vector.Unboxed (Vector)
import qualified Data.Vector.Unboxed as V
import Data.Bits
import Data.Word
import Numeric
import Conway

toBits :: Word8 -> [Bool]
toBits c = map (testBit c) [0..7]

toField :: ByteString -> Field
toField bs = Field bits 128 rows
    where dat = BS.unpack bs
          rows = ceiling (fromIntegral (length dat) / 16)
          dat' = take (rows * 16) (dat ++ repeat 0)
          bits = V.fromList $ concatMap toBits dat'

compress :: Field -> Vector Bool
compress f@Field{..} = V.generate 128 gen
    where gen i = V.foldl1' xor $ V.generate height (getAt f i)

bitsToInteger :: Vector Bool -> Integer
bitsToInteger = V.ifoldl (\n i x -> if x then bit i + n else n) 0

toHex :: Vector Bool -> Text
toHex = Text.pack . ($ "") . showHex . bitsToInteger

hash :: Text -> Text
hash = toHex . compress . (!! 100) . conway . toField . Text.encodeUtf8
