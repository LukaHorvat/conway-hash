Conway hashing. You take a string, convert it into bits, fit them in a 128xN array, run a 100 generations of Conway's Game of Life, XOR all the rows and convert them back to a hex string.

It's disgustingly slow (for my naive implementation of CGoL) but it does work.

```
ghci: hash "hello  world"
"1fb04367011800822e852030f0001020"
ghci: hash "hello world"
"249c01dfbb9f8e7723170e8000810c49"
ghci: hash "hello worlb"
"219b0420e00fdfb808080f1e4a400380"
ghci: hash "hello worlc"
"80020424f004400a5c0443fe723273b"
```

There are some pretty obvious patterns forming, which isn't ideal, but I'm thinking these could be eliminated with more generations and maybe a smarter default field instead of just an empty one.

I've also found some strings for which the result is 0.